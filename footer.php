<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Pidie_Ui
 */

?>

	</div><!-- #content -->

	<div class="pd-social-bar">
		<div class="pd-social-item" data-type="share" data-name="facebook" data-url="<?php pidie_ui_og_post() ?>"></div>
		<div class="pd-social-item" data-type="share" data-name="twitter" data-url="<?php pidie_ui_og_post() ?>" data-title="<?php pidie_ui_og_post('title') ?>" data-description="<?php pidie_ui_og_post('description') ?>"></div>
		<div class="pd-social-item" data-type="share" data-name="pinterest" data-url="<?php pidie_ui_og_post() ?>" data-title="<?php pidie_ui_og_post('title') ?>" data-description="<?php pidie_ui_og_post('description') ?>"></div>
		<div class="pd-social-item" data-type="share" data-name="google" data-url="<?php pidie_ui_og_post() ?>"></div>
		<div class="pd-social-item" data-type="share" data-name="tumblr" data-url="<?php pidie_ui_og_post() ?>" data-title="<?php pidie_ui_og_post('title') ?>" data-description="<?php pidie_ui_og_post('description') ?>"></div>
		<div class="pd-social-item" data-type="share" data-name="reddit" data-url="<?php pidie_ui_og_post() ?>" data-title="<?php pidie_ui_og_post('title') ?>"></div>
	</div>

	<footer id="colophon" class="site-footer pd-footer">
		<div class="pd-container">
			<div class="pd-row">
				<div class="pd-col-lg-3 pd-col-md-6 pd-col-sm-6 pd-xs-12">
					<?php
					if(is_active_sidebar('footer')){
						dynamic_sidebar('footer');
					}
					?>
				</div>
				<div class="pd-col-lg-3 pd-col-md-6 pd-col-sm-6 pd-xs-12">
					<?php
					if(is_active_sidebar('footer-2')){
						dynamic_sidebar('footer-2');
					}
					?>
				</div>
				<div class="pd-col-lg-3 pd-col-md-6 pd-col-sm-6 pd-xs-12">
					<?php
					if(is_active_sidebar('footer-3')){
						dynamic_sidebar('footer-3');
					}
					?>
				</div>
				<div class="pd-col-lg-3 pd-col-md-6 pd-col-sm-6 pd-xs-12">
					<?php
					if(is_active_sidebar('footer-4')){
						dynamic_sidebar('footer-4');
					}
					?>
				</div>
			</div>
		</div>
		<div class="site-info pd-footer-info">
			<div class="pd-container">
				<div class="pd-row">
					<div class="pd-col">
						<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'pidie-ui' ) ); ?>">
							<?php
							/* translators: %s: CMS name, i.e. WordPress. */
							printf( esc_html__( 'Proudly powered by %s', 'pidie-ui' ), 'WordPress' );
							?>
						</a>
					</div>
					<div class="pd-col pd-right">
						<?php
						/* translators: 1: Theme name, 2: Theme author. */
						printf( esc_html__( 'Theme: %1$s by %2$s.', 'pidie-ui' ), 'pidie-ui', '<a href="http://underscores.me/">Underscores.me</a>' );
						?>
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<div class="pd-back-to-top hidden"><i class="pd-icon-backtop"></i></div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
