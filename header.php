<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Pidie_Ui
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<meta property="og:url" content="<?php pidie_ui_og_post() ?>">
	<meta property="og:title" content="<?php pidie_ui_og_post('title') ?>">
	<meta property="og:description" content="<?php pidie_ui_og_post('description') ?>">
	<meta property="og:image" content="<?php pidie_ui_og_post('image') ?>">

	<?php wp_head(); ?>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<nav class="pd-nav">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="pd-nav-brand">
			<?php if(is_singular()): ?>
				<h1><i class="pd-icon-arrow-left"></i></h1>
			<?php else: ?>
				<h1><?php bloginfo( 'name' ); ?></h1>
			<?php endif; ?>
		</a>
		<button type="button" class="pd-nav-toggle" data-target="#navcollapse">
			<span class="pd-icon-bar"></span>
		</button>
		<div class="pd-nav-collapse" id="navcollapse">
			<div class="pd-nav-left pd-nav-dropdown">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</div>
			<div class="pd-nav-right">
				<ul>
					<li class="pd-nav-search mt-none">
						<form method="get" style="margin-top: 0;">
							<input type="search" name="s"/>
							<button type="submit"><i class="pd-icon-search"></i></button>
						</form>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<?php pidie_ui_single_thumbnail() ?>

	<div id="content" class="site-content pd-container pd-content">
